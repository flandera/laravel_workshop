@extends('layouts.app')
@section('content')
    <div class="h1">
        Banán až do domu
    </div>
    <div class="products">
        @foreach($products as $product)
            <div class="product">
                <div class="image">

                </div>
                <div class="description">
                    <span>{{$product->name}}</span>
                    <span>{{$product->description}}</span>
                    <span>{{$product->price}} Kč</span>
                    {!! Form::model($order, $route) !!}
                    {!! Form::hidden('product', $product->id) !!}
                    <div class="form-group">
                        {!! Form::submit('Koupit', ['class'=>'btn btn-primary form-control']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        @endforeach
    </div>
@endsection
