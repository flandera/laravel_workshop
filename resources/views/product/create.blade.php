@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <h1>Vytvoř produkt</h1>
        </div>
        <div class="panel-body">
            <form method="POST" action="{{url('/products')}}">
                {{ Form::token() }}
                <div class="form-group">
                    {!! Form::label('name', 'Název:') !!}
                    {!! Form::text('name', null, ['class'=>'form-control']) !!}
               </div>
                <div class="form-group">
                    {!! Form::label('description', 'Popis:') !!}
                    {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
               </div>
                <div class="form-group">
                    {!! Form::label('price', 'Cena:') !!}
                    {!! Form::number('price', null, ['class'=>'form-control', 'step' => 'any', 'lang'=>"cs"]) !!}
                </div>
                <div class="form-check">
                    {!! Form::label('active', 'Aktivní:') !!}
                    {!! Form::hidden('active', 0) !!}
                    {!! Form::checkbox('active', 1, true) !!}
               </div>
                {!! Form::submit('Uložit', ['class'=>'btn btn-primary form-control']) !!}
            </form>
        </div>
    </div>
@endsection
