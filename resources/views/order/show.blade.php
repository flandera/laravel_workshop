@extends('layouts.app')
@section('content')
    <h1>Nová objednávka</h1>
    <span>číslo: {{$order->id}}</span>
    @foreach($products as $product)
        <div class="product">
            <span>{{$product->name}}</span>
            <span>{{$product->price}}</span>
        </div>
    @endforeach
    @if($order->status->name === 'nová')
    {!! Form::model($order, ['method'=>'PATCH', 'route' => ['objednavky.update', $order->id]]) !!}
    {!! Form::hidden('status', '2') !!}
    <div class="form-group">
        {!! Form::submit('Odeslat', ['class'=>'btn btn-primary form-control']) !!}
    </div>
    {!! Form::close() !!}
    @else
        <div class="form-group">
            {!! Form::submit('Odeslaná', ['class'=>'btn btn-standard form-control', 'disabled' => 'disabled']) !!}
        </div>
    @endif
@endsection
