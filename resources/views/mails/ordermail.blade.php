<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<body>
    <div>
        <span>Dobrý den, {{$user->name}}<br></span>
        <span>Vaše objednávka byla odeslána ke zpracování.</span>
        <h3>Souhrn objednávky:</h3>
        @foreach($products as $product)
            <div class="product">
                <span>{{$product->name}}</span>
                <span>{{$product->price}}</span>
            </div>
        @endforeach
        <span>Děkujeme, Váš Banán až do domu</span>
    </div>
</body>
</html>
