<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function (Blueprint $table) {
			$table->increments('id');
			$table->string('session_id');
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->ondelete('cascade');
			$table->integer('status_id')->unsigned();
			$table->foreign('status_id')->references('id')->on('statuses')->ondelete('cascade');
			$table->timestamps();
		});

		Schema::create('order_product', function (Blueprint $table){
			$table->increments('id');
			$table->integer('order_id')->unsigned();
			$table->foreign('order_id')->references('id')->on('orders')->ondelete('cascade');
			$table->integer('product_id')->unsigned();
			$table->foreign('product_id')->references('id')->on('products')->ondelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('order_product');
		Schema::dropIfExists('orders');
	}
}
