<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $users[1] = new App\User(['name'=>'Admin','email'=>'admin@eshop.cz', 'password' => bcrypt('admin')]);
	    $users[2] = new App\User(['name'=>'Ucto','email'=>'ucto@eshop.cz', 'password' => bcrypt('ucto')]);
	    $users[3] = new App\User(['name'=>'Zakaznik','email'=>'zakaznik@eshop.cz', 'password' => bcrypt('zakaznik')]);


	    foreach($users as $key => $user){
		    /**@var App\User $user*/
	    	$user->save();
		    $user->roles()->attach($key);
	    }
    }
}
