<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $roles[] = new App\Role(['name'=>'admin','description'=>'administrátor']);
	    $roles[] = new App\Role(['name'=>'account','description'=>'účetní']);
	    $roles[] = new App\Role(['name'=>'customer','description'=>'zákazník']);
	    foreach($roles as $role){
		    $role->save();
	    }
    }
}
