<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $statuses[] = new \App\Status(['id' => 1, 'name'=>'nová','description'=>'právě založená']);
	    $statuses[] = new \App\Status(['id' => 2, 'name'=>'odeslaná','description'=>'odeslaná k vyřízení']);
	    $statuses[] = new \App\Status(['id' => 3, 'name'=>'opuštěná','description'=>'zákazník z odešel a nedokončil ji']);
	    foreach($statuses as $status){
		    $status->save();
	    }
    }
}
