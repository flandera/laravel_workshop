<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $products[] = new App\Product(['name' => 'Pomeranč', 'description' => 'Kubánský', 'price' => 5]);
	    $products[] = new App\Product(['name' => 'Banán', 'description' => 'žlutý', 'price' => 9.9]);
	    $products[] = new App\Product(['name' => 'Meloun', 'description' => 'vodní', 'price' => 6.9]);
	    foreach($products as $product){
		    $product->save();
	    }
    }
}
