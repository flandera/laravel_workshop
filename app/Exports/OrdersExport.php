<?php

namespace App\Exports;

use App\Order;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class OrdersExport implements FromQuery, WithMapping, WithHeadings
{
    use Exportable;

	/**
	 * @var \Carbon\Carbon
	 */
	private $day;

	public function __construct()
    {
	    $this->day =  $yesterday = Carbon::today()->modify('- 1 DAY')->startOfDay();
    }

	/**
	 * @return \Illuminate\Database\Query\Builder
	 */
	public function query()
    {
	   return Order::where('created_at', '>=', $this->day)->with('user', 'status', 'products');
    }

	/**
	 * @param Order $order
	 * @return array
	 */
	public function map($order): array
	{
		return[
			$order->id,
			$order->user->name,
			$order->user->email,
			$order->user->street,
			$order->user->city,
			$order->user->zip_code,
			$order->status->name,
			json_encode($order->products),
			$order->created_at,
			$order->updated_at
		];
	}

	/**
	 * @return array
	 */
	public function headings(): array
	{
		return [
			'id',
			'user_name',
			'user_email',
			'user_street',
			'user_city',
			'user_zip_code',
			'status',
			'products',
			'created_at',
			'updated_at'
		];
    }
}
