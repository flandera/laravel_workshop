<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function orders()
	{
		return $this->hasMany(Order::class);
    }
}
