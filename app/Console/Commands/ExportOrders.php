<?php

namespace App\Console\Commands;

use App\Exports\OrdersExport;
use App\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class ExportOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export orders for previous day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $date = Carbon::today()->modify('- 1 DAY')->format('Y-m-d');
    	return (new OrdersExport)->store('orders_'.$date.'.csv');
    }
}
