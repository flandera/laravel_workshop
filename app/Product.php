<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
	    'name', 'description', 'price', 'active'
    ];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function orders(){
		return $this->belongsToMany(Order::class)->withTimestamps();
	}
}
