<?php

namespace App\Mail;

use App\Order;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderEmail extends Mailable
{
    use Queueable, SerializesModels;

	/**
	 * @var \App\Order
	 */
	private $order;

	/**
     * Create a new message instance.
     * @param \App\Order $order
     * @return void
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $order = $this->order;
        $user = User::where('id', $order->user->id)->first();
        $products = $order->products()->get(['name', 'price']);
    	return $this->subject('Objednávka úspěšně odeslána')->view('mails.ordermail', compact('order', 'user', 'products'));
    }
}
