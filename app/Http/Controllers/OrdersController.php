<?php

namespace App\Http\Controllers;

use App\Mail\OrderEmail;
use App\Order;
use App\Product;
use App\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('order.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//	    return view('order.show');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $product = Product::where('id', $request['product'])->first();
	    $session_id = session()->getId();
	    $status = Status::where('name', 'nová')->first();
	    $user = $request->user();
		$order = $request['order'];
	    if(is_null($order)){
			$order = new Order;
		}
		$order->session_id = $session_id;
		$order->user()->associate($user);
	    $order->status()->associate($status);
	    $order->save();
	    $order->products()->save($product);
		$products = $order->products()->get();
    	return view('order.show', compact('order', 'products'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    $order = Order::where('id', $id)->first();
    	$product = Product::where('id', $request['product'])->first();
    	if(!is_null($product)){
		    $order->products()->save($product);
	    }
	    $status = Status::where('id', $request['status'])->first();
    	if(!is_null($status)){
    		$order->status()->associate($status);
	    }
	    $order->save();
	    $products = $order->products()->get();
	    if($order->status->name === 'odeslaná'){
	    	Mail::to($order->user->email)->send(new OrderEmail($order));
	    }
	    return view('order.show', compact('order', 'products'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
