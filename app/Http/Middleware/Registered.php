<?php

namespace App\Http\Middleware;

use Closure;

class Registered
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if($user){
	        return $next($request);
        }
		flash()->error('Pro tuto akci musíte být přihlášen');
        return redirect('login');

    }
}
